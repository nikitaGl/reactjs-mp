const path = require('path');
const selenium = require('selenium-download');

selenium.ensure(path.join(__dirname, '..', 'bin'), function (error) {
	if (error) console.error(error.stack);
	process.exit(0);
});