const merge = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');
const common = require('./webpack.config.common');

module.exports = merge(common, {
  name: 'server',
  target: 'node',
  entry: ['babel-polyfill', './app/serverRenderer.jsx'],
  externals: [nodeExternals()],
  output: {
    filename: 'js/serverRenderer.js',
    libraryTarget: 'commonjs2',
  },
	module: {
		rules: [
			{
				test: /\.scss$/,
				loader: 'css-loader/locals!sass-loader'
			},
		],
	},

});
