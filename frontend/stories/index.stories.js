import React from 'react';

import { storiesOf } from '@storybook/react';
import { linkTo } from '@storybook/addon-links';
import { MemoryRouter } from 'react-router';

import { Button, Welcome } from '@storybook/react/demo';

import MoviesResponse from '../app/__mocks__/movies-resp';
import MovieSearch from '../app/__mocks__/movie-search';
import MovieList from '../app/pages/movie/components/movie-list/movie-list';
import FilmInfo from '../app/pages/film/components/film-header/components/film-info/film-info';
import MovieHeader from '../app/pages/movie/components/movie-header/movie-header';
import FilmHeader from '../app/pages/film/components/film-header/film-header';

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

const movies = MoviesResponse.data;
const [ movie ] = movies;
storiesOf('Movie', module)
	.addDecorator(story => (
		<MemoryRouter initialEntries={['/']}>
        {story()}
    </MemoryRouter>
	))
  .add('Card-List', () => <MovieList movies={movies} />)
  .add('Film-info', () => <FilmInfo filmInfo={movie} />);

const { searchFilters, moviesSort, moviesCount} = MovieSearch;
storiesOf('Headers', module)
	.addDecorator(story => (
		<MemoryRouter initialEntries={['/']}>
			{story()}
		</MemoryRouter>
	))
	.add('Movie-Header', () => <MovieHeader searchFilters={searchFilters}
																					moviesCount={moviesCount}
																					moviesSort={moviesSort} />)
	.add('Film-Header', () => <FilmHeader filmInfo={movie} />);