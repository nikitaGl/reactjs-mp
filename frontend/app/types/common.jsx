// @flow

declare type IActionList = {
	[func: string]: () => any,
};