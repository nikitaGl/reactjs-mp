// @flow

declare type IFilm = {
	filmData: IFilmData,
};

declare type IFilmData = {
	film: IMovie,
	loading: boolean,
	hasError: boolean,
};

declare type IMovieList = {
	moviesData: IMoviesData,
	moviesSort: IMoviesSort,
	searchFilters: ISearchFilter,
};

declare type IMoviesData = {
	movies: Array<IMovie>,
	loading: boolean,
	hasError: boolean,
};
