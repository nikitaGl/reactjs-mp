// @flow

declare type ILabelValue = {
	label: string,
	value: string,
};

declare type ISearchFilter = {
	activeFilter: ILabelValue,
	filters: Array<ILabelValue>,
};

declare type IMoviesSort = {
	selectedFilter: ILabelValue,
	filters: Array<ILabelValue>,
};