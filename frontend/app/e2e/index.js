module.exports = {
	'main': browser => {
		browser.url('http://localhost:8080')
			.waitForElementVisible('.movie-list', 1000);

		browser.assert.elementPresent('.movie-search__search-field');

		browser.setValue('.movie-search__search-field', 'Nightwatch');

		browser.click('.movie-search__search-button');

		browser.end();
	}
};