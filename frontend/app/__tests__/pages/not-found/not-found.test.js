import React from 'react';
import { shallow } from 'enzyme';

import NotFoundPage from '../../../pages/not-found/not-found';

describe('<NotFoundPage />', () => {
	it('renders without errors', () => {
		const component = shallow(<NotFoundPage/>);
		expect(component).toMatchSnapshot();
	});
});