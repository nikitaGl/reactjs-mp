import React from 'react';
import { shallow } from 'enzyme';

import NoMovies from '../../../../../pages/movie/components/no-movies/no-movies';

describe('<NoMovies />', () => {
	it('renders without errors', () => {
		const component = shallow(<NoMovies/>);
		expect(component).toMatchSnapshot();
	});
});