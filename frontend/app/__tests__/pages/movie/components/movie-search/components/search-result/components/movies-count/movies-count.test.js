import React from 'react';
import { shallow } from 'enzyme';

import MoviesCount from '../../../../../../../../../pages/movie/components/movie-search/components/search-result/components/movies-count/movies-count';

describe('<MoviesCount />', () => {
	it('renders without errors', () => {
		const moviesCount = 5;
		const component = shallow(<MoviesCount moviesCount={moviesCount}/>);
		expect(component).toMatchSnapshot();
	});
});