import React from 'react';
import { mount, shallow } from 'enzyme';

import MoviesSearch from '../../../../../../../../../__mocks__/movie-search';
import MoviesSort from '../../../../../../../../../pages/movie/components/movie-search/components/search-result/components/movies-sort/movies-sort';

const setup = () => {
	const actions = {
		selectSorting: jest.fn(),
	};
  const enzymeWrapper = mount(<MoviesSort moviesSort={MoviesSearch.moviesSort} actions={actions} />);

  return {
		actions,
		enzymeWrapper,
	};
};

describe('<MoviesSort />', () => {
	it('renders without errors', () => {
		const component = shallow(<MoviesSort moviesSort={MoviesSearch.moviesSort} />);
		expect(component).toMatchSnapshot();
	});

	it('should be only one active element', () => {
		const component = shallow(<MoviesSort moviesSort={MoviesSearch.moviesSort} />);
		expect(component.find('.movies-sort__filter--active').length).toBe(1);
	});

	it('should handle updateSearchField change', () => {
		const { enzymeWrapper, actions } = setup();
		const sortBy = enzymeWrapper.find('div.movies-sort__filter').not('.movies-sort__filter--active').at(0);
		sortBy.props().onClick(MoviesSort[1]);
		expect(actions.selectSorting.mock.calls.length).toBe(1);
	});
});