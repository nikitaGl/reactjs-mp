import React from 'react';
import { shallow } from 'enzyme';

import MoviesSearch from '../../../../../../../__mocks__/movie-search';
import SearchFilters from '../../../../../../../pages/movie/components/movie-search/components/search-filters/search-filters';
import { mount } from 'enzyme/build/index';

const setup = () => {
	const actions = {
		selectFilter: jest.fn(),
	};
	const { searchFilters } = MoviesSearch;
	const enzymeWrapper = mount(<SearchFilters searchFilters={searchFilters} actions={actions} />);

	return {
		actions,
		enzymeWrapper,
	};
};

describe('<SearchFilters />', () => {
	it('renders without errors', () => {
		const { searchFilters } = MoviesSearch;
		const component = shallow(<SearchFilters searchFilters={searchFilters} />);
		expect(component).toMatchSnapshot();
	});

	it('should be only one active element', () => {
		const { searchFilters } = MoviesSearch;
		const component = shallow(<SearchFilters searchFilters={searchFilters} />);
		expect(component.find('.button').length - component.find('.button--inactive').length).toBe(1);
	});

	it('should handle selectFilter click', () => {
		const { enzymeWrapper, actions } = setup();
		const filter = enzymeWrapper.find('button.button.button--inactive').at(0);
		filter.props().onClick(SearchFilters[1]);
		expect(actions.selectFilter.mock.calls.length).toBe(1);
	});
});