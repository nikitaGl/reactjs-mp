import React from 'react';
import { shallow } from 'enzyme';

import MoviesSearch from '../../../../../../../__mocks__/movie-search';
import SearchResult from '../../../../../../../pages/movie/components/movie-search/components/search-result/search-result';

describe('<SearchResult />', () => {
	it('renders without errors', () => {
		const component = shallow(<SearchResult searchResult={{moviesCount: MoviesSearch.moviesCount}} moviesSort={MoviesSearch.moviesSort} />);
		expect(component).toMatchSnapshot();
	});
});