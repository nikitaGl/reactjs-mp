import React from 'react';
import { shallow } from 'enzyme';

import MovieSearch from '../../../../../pages/movie/components/movie-search/movie-search';
import MoviesSearch from '../../../../../__mocks__/movie-search';
import { mount } from 'enzyme/build/index';
import MoviesSort from '../../../../../pages/movie/components/movie-search/components/search-result/components/movies-sort/movies-sort';

const setup = () => {
	const actions = {
		moviesFetchData: jest.fn(),
	};
	const { searchFilters, moviesCount, moviesSort } = MoviesSearch;
	const enzymeWrapper = mount(<MovieSearch actions={actions}
																					 searchFilters={searchFilters}
																					 moviesCount={moviesCount}
																					 moviesSort={moviesSort}/>);

	return {
		actions,
		enzymeWrapper,
	};
};

describe('<MovieSearch />', () => {
	it('renders without errors', () => {
		const { searchFilters, moviesCount, moviesSort } = MoviesSearch;
		const component = shallow(<MovieSearch searchFilters={searchFilters}
																					 moviesCount={moviesCount}
																					 moviesSort={moviesSort}/>);
		expect(component).toMatchSnapshot();
	});

	it('should handle selectSorting click', () => {
		const { enzymeWrapper } = setup();
		const searchInput = enzymeWrapper.find('input.movie-search__search-field').at(0);
		const fakeEvent = {
			target: {
				value: 'test',
			},
		};
		searchInput.props().onChange(fakeEvent);
		expect(enzymeWrapper.state('searchField')).toBe('test');
	});

	it('should handle doSearch click', () => {
		const { enzymeWrapper, actions } = setup();
		const searchButton = enzymeWrapper.find('button.button.movie-search__search-button').at(0);
		searchButton.props().onClick();
		expect(actions.moviesFetchData.mock.calls.length).toBe(1);
	});
});