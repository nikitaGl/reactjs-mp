import React from 'react';
import { shallow } from 'enzyme';

import MovieList from '../../../../../pages/movie/components/movie-list/movie-list';
import MoviesResponse from '../../../../../__mocks__/movies-resp';

describe('<MovieList />', () => {
	it('renders without errors', () => {
		const component = shallow(<MovieList movies={MoviesResponse.data}/>);
		expect(component).toMatchSnapshot();
	});

	it('should render all elements', () => {
		const component = shallow(<MovieList movies={MoviesResponse.data}/>);
		expect(component.find('MovieCard').length).toBe(MoviesResponse.data.length);
	});
});