import React from 'react';
import { shallow } from 'enzyme';

import MovieHeader from '../../../../../pages/movie/components/movie-header/movie-header';

describe('<MovieHeader />', () => {
	it('renders without errors', () => {
		const component = shallow(<MovieHeader/>);
		expect(component).toMatchSnapshot();
	});
});