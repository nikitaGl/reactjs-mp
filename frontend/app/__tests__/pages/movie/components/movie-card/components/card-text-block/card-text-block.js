import React from 'react';
import { shallow } from 'enzyme';

import CardTextBlock from '../../../../../../../pages/movie/components/movie-card/components/card-text-block/card-text-block';
import MoviesResponse from '../../../../../../../__mocks__/movies-resp';

describe('<CardTextBlock />', () => {
	it('renders without errors', () => {
		const movie = MoviesResponse.data[0];
		const info = {
			title: movie.title,
			date: movie.release_date,
			genres: movie.genres
		};
		const component = shallow(<CardTextBlock info={info}/>);
		expect(component).toMatchSnapshot();
	});
});