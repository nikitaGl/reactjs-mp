import React from 'react';
import { shallow } from 'enzyme';

import MovieCard from '../../../../../pages/movie/components/movie-card/movie-card';
import MoviesResponse from '../../../../../__mocks__/movies-resp';

describe('<MovieCard />', () => {
	it('renders without errors', () => {
		const component = shallow(<MovieCard movie={MoviesResponse.data[0]}/>);
		expect(component).toMatchSnapshot();
	});
});