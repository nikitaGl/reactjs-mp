import React from 'react';
import { Provider } from 'react-redux';
import { shallow, render } from 'enzyme';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk';

import MoviePageConnected, { MoviePage } from '../../../pages/movie/movie';
import { Movies, NoMovies, MoviesAreLoading } from '../../../__mocks__/movies';
import { ReducersDefault } from '../../../__mocks__/reducers';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const actions = {
	moviesFetchData: jest.fn(),
};

describe('<MoviePage />', () => {
	it('renders without errors', () => {
		const component = shallow(<MoviePage actions={actions} movies={Movies} />);
		expect(component).toMatchSnapshot();
	});

	it('renders without errors when no movies found', () => {
		const component = shallow(<MoviePage actions={actions} movies={NoMovies} />);
		expect(component).toMatchSnapshot();
	});

	it('renders without errors when movies are loading', () => {
		const component = shallow(<MoviePage actions={actions} movies={MoviesAreLoading} />);
		expect(component).toMatchSnapshot();
	});

	it('renders connected component', () => {
		const store = mockStore(ReducersDefault);
		const component = render(
			<Provider store={store}>
				<MoviePageConnected />
			</Provider>
		);
		expect(component).toMatchSnapshot();
	});
});