import React from 'react';
import { shallow } from 'enzyme';

import AppPage from '../../../pages/app/app';

describe('<AppPage />', () => {
	it('renders without errors', () => {
		const component = shallow(<AppPage/>);
		expect(component).toMatchSnapshot();
	});
});