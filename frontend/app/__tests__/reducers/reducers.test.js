import * as types from '../../const/action-types';
import reducer from '../../reducers';
import searchFilterReducer from '../../reducers/movie/search-filters';
import movieSortReducer from '../../reducers/movie/movies-sort';
import { moviesHasError, moviesAreLoading, movies } from '../../reducers/movie/movie';
import { ReducersDefault, SearchFilterReducerDefault, MovieSortReducerDefault } from '../../__mocks__/reducers';
import SearchFilters from '../../const/search-filters';
import MoviesSort from '../../const/movies-sort';
import Movies from '../../__mocks__/movies-resp';

describe('reducers', () => {
	it('should return the initial state', () => {
		expect(reducer(undefined, {})).toEqual(ReducersDefault);
	});

	it('should handle SELECT_SEARCH_FILTER', () => {
		const activeFilter = SearchFilters[1];
		expect(searchFilterReducer(SearchFilterReducerDefault, {
			type: types.SELECT_SEARCH_FILTER,
			activeFilter,
		}).activeFilter).toEqual(activeFilter);
	});

	it('should handle SELECT_MOVIE_SORT', () => {
		const selectedFilter = MoviesSort[1];
		expect(movieSortReducer(MovieSortReducerDefault, {
			type: types.SELECT_MOVIE_SORT,
			selectedFilter,
		}).selectedFilter).toEqual(selectedFilter);
	});

	it('should handle MOVIES_HAS_ERROR', () => {
		const hasError = true;
		expect(moviesHasError(false, {
			type: types.MOVIES_HAS_ERROR,
			hasError,
		})).toEqual(true);
	});

	it('should handle MOVIES_ARE_LOADING', () => {
		const isLoading = true;
		expect(moviesAreLoading(false, {
			type: types.MOVIES_ARE_LOADING,
			isLoading,
		})).toEqual(true);
	});

	it('should handle MOVIES_FETCH_DATA_SUCCESS', () => {
		const moviesData = Movies.data;
		expect(movies([], {
			type: types.MOVIES_FETCH_DATA_SUCCESS,
			movies: moviesData,
		})).toEqual(moviesData);
	});
});
