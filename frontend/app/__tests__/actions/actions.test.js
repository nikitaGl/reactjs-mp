import fetchMock from 'fetch-mock/es5/server';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk';

import * as actions from '../../actions';
import * as types from '../../const/action-types';
import MovieSearch from '../../__mocks__/movie-search';
import Movies from '../../__mocks__/movies-resp';
import Config from '../../config/endpoints';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('actions', () => {
	it('should create an action - selectFilter', () => {
		const activeFilter = MovieSearch.searchFilters.activeFilter;
		const expectedAction = {
			type: types.SELECT_SEARCH_FILTER,
			activeFilter: activeFilter,
		};
		expect(actions.selectFilter(activeFilter)).toEqual(expectedAction);
	});

	it('should create an action - selectSorting', () => {
		const selectedFilter = MovieSearch.moviesSort.selectedFilter;
		const expectedAction = {
			type: types.SELECT_MOVIE_SORT,
			selectedFilter: selectedFilter,
		};
		expect(actions.selectSorting(selectedFilter)).toEqual(expectedAction);
	});

	it('should create an action - moviesHasError', () => {
		const hasError = true;
		const expectedAction = {
			type: types.MOVIES_HAS_ERROR,
			hasError,
		};
		expect(actions.moviesHasError(hasError)).toEqual(expectedAction);
	});

	it('should create an action - moviesAreLoading', () => {
		const isLoading = true;
		const expectedAction = {
			type: types.MOVIES_ARE_LOADING,
			isLoading,
		};
		expect(actions.moviesAreLoading(isLoading)).toEqual(expectedAction);
	});

	it('should create an action - moviesFetchDataSuccess', () => {
		const movies = Movies.data;
		const expectedAction = {
			type: types.MOVIES_FETCH_DATA_SUCCESS,
			movies,
		};
		expect(actions.moviesFetchDataSuccess(movies)).toEqual(expectedAction);
	});
});


describe('async actions', () => {
	afterEach(() => {
		fetchMock.reset();
		fetchMock.restore();
	});

	it('should create an action - MOVIES_FETCH_DATA_SUCCESS when fetching movies has been done', () => {
		fetchMock
			.getOnce(`${Config.baseUrl}${Config.movies}`, { body: Movies, headers: { 'content-type': 'application/json' } });

		const movies = Movies.data;
		const expectedActions = [
			{
				type: types.MOVIES_ARE_LOADING,
				isLoading: true,
			},
			{
				type: types.MOVIES_FETCH_DATA_SUCCESS,
				movies,
			},
			{
				type: types.MOVIES_ARE_LOADING,
				isLoading: false,
			},
		];

		const store = mockStore();
		return store.dispatch(actions.moviesFetchData(`${Config.baseUrl}${Config.movies}`)).then(() => {
			expect(store.getActions()).toEqual(expectedActions)
		});
	});

	it('should create an action - MOVIES_HAS_ERROR when fetching movies has been done', () => {
		fetchMock
			.getOnce(`${Config.baseUrl}${Config.movies}`, 404);

		const expectedActions = [
			{
				type: types.MOVIES_ARE_LOADING,
				isLoading: true,
			},
			{
				type: types.MOVIES_HAS_ERROR,
				hasError: true,
			},
			{
				type: types.MOVIES_ARE_LOADING,
				isLoading: false,
			},
		];

		const store = mockStore();
		return store.dispatch(actions.moviesFetchData(`${Config.baseUrl}${Config.movies}`)).then(() => {
			expect(store.getActions()).toEqual(expectedActions)
		});
	});
});