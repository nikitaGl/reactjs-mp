import React from 'react';
import { shallow } from 'enzyme';

import Header from '../../../components/header/header';

describe('<Header />', () => {
	it('renders without errors', () => {
		const component = shallow(<Header/>);
		expect(component).toMatchSnapshot();
	});
});