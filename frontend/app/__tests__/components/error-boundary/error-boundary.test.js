import React from 'react';
import { shallow } from 'enzyme';

import ErrorBoundary from '../../../components/error-boundary/error-boundary';

describe('<ErrorBoundary />', () => {
	it('renders without errors', () => {
		const component = shallow(
			<ErrorBoundary>
				<div>Hello world</div>
			</ErrorBoundary>
		);
		expect(component).toMatchSnapshot();
	});

	it('renders with error state', () => {
		const component = shallow(
			<ErrorBoundary>
				<div>Hello world</div>
			</ErrorBoundary>
		);

		component.instance().componentDidCatch();
		component.update();

		expect(component).toMatchSnapshot();
	});
});