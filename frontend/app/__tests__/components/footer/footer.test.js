import React from 'react';
import { shallow } from 'enzyme';

import FooterComponent from '../../../components/footer/footer';

describe('<Footer />', () => {
	it('renders without errors', () => {
		const component = shallow(FooterComponent);
		expect(component).toMatchSnapshot();
	});
});