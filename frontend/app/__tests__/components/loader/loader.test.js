import React from 'react';
import { shallow } from 'enzyme';

import Loader from '../../../components/loader/loader';

describe('<Loader />', () => {
	it('renders without errors', () => {
		const component = shallow(<Loader/>);
		expect(component).toMatchSnapshot();
	});
});