import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import Root from '../app/pages/app/app';
import configureStore from '../app/reducers/configureStore';

function renderHTML(html, preloadedState) {
  return `
      <!doctype html>
      <html>
        <head>
          <meta charset=utf-8>
          <title>React Mentoring</title>
          ${process.env.NODE_ENV === 'development' ? '' : '<link href="/css/main.css" rel="stylesheet" type="text/css">'}
        </head>
        <body>
          <div id="app">${html}</div>
          <script>
            // WARNING: See the following for security issues around embedding JSON in HTML:
            // http://redux.js.org/docs/recipes/ServerRendering.html#security-considerations
            window.PRELOADED_STATE = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
          </script>
          <script src="/js/main.js"></script>
        </body>
      </html>
  `;
}

export default function serverRenderer() {
	return (req, res) => {
		const store = configureStore();
		// This context object contains the results of the render
		const context = {};

		const root = (
			<Root
				context={context}
				location={req.url}
				Router={StaticRouter}
				store={store}
			/>
		);

		store.runSaga().done.then(() => {
			const htmlString = renderToString(root);

			// context.url will contain the URL to redirect to if a <Redirect> was used
			if (context.url) {
				res.writeHead(302, {
					Location: context.url,
				});
				res.end();
				return;
			}

			const preloadedState = store.getState();

			res.send(renderHTML(htmlString, preloadedState));
		});

		// Do first render, starts initial actions.
		renderToString(root);
		// When the first render is finished, send the END action to redux-saga.
		store.close();
	};
}
