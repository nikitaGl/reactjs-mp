import {
	SELECT_SEARCH_FILTER, SELECT_MOVIE_SORT,
	MOVIES_FETCH_DATA, MOVIES_FETCH_DATA_SUCCESS, MOVIES_HAS_ERROR,
	FILM_FETCH_DATA, FILM_FETCH_DATA_SUCCESS, FILM_HAS_ERROR
} from '../const/action-types';

export const selectFilter = (activeFilter) => ({ type: SELECT_SEARCH_FILTER, activeFilter });
export const selectSorting = (selectedFilter) => ({ type: SELECT_MOVIE_SORT, selectedFilter });

export const moviesHasError = (state) => ({ type: MOVIES_HAS_ERROR, hasError: state });
export const moviesFetchData = (query) => ({ type: MOVIES_FETCH_DATA, query });
export const moviesFetchDataSuccess = (movies) => ({ type: MOVIES_FETCH_DATA_SUCCESS,	movies});

export const filmHasError = (state) => ({ type: FILM_HAS_ERROR, hasError: state });
export const filmFetchData = (id) => ({ type: FILM_FETCH_DATA, id });
export const filmFetchDataSuccess = (film) => ({ type: FILM_FETCH_DATA_SUCCESS,	film });

// export const filmFetchData = (url, moviesUrl) => {
// 	return (dispatch) => {
// 		dispatch(filmIsLoading(true));
// 		return fetch(url)
// 			.then((response) => {
// 				if (!response.ok) {
// 					throw Error(response.statusText);
// 				}
// 				return response;
// 			})
// 			.then((response) => response.json())
// 			.then((item) => {
// 				dispatch(filmFetchDataSuccess(item));
// 				const [genre] = item.genres;
// 				const query = new URLSearchParams();
// 				query.set('searchBy', 'genres');
// 				query.set('search', genre);
// 				dispatch(moviesFetchData(query.toString()));
// 			})
// 			.catch(() => {
// 				dispatch(filmHasError(true))
// 			})
// 			.finally(() => {
// 				dispatch(filmIsLoading(false));
// 			});
// 	};
// };