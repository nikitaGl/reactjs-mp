import createBrowserHistory  from 'history/createBrowserHistory';

export const addQueryParams = (query) => {
	if (typeof window !== 'undefined') {
		const history = createBrowserHistory();
		const newLocation = Object.assign({}, history.location);
		const searchParams = new URLSearchParams(newLocation.search);
		Object.keys(query).forEach((key) => searchParams.set(key, query[key]));
		newLocation.search = searchParams.toString();
		history.push(newLocation);
	}
};