import MoviesResponse from './movies-resp';
import MovieSearch from './movie-search';

const defaultState = {
	moviesHasError: false,
	moviesAreLoading: false,
	movies: MoviesResponse.data,
	searchFilters: MovieSearch.searchFilters,
	moviesSort: MovieSearch.moviesSort,
};

export const Movies = {
	...defaultState,
};

export const NoMovies = {
	...defaultState,
	movies: [],
};

export const MoviesAreLoading = {
	...defaultState,
	moviesAreLoading: true,
};