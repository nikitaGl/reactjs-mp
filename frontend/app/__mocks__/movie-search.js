import MoviesSort from '../const/movies-sort';

const MovieSearch = {
	searchFilters: {
		filters: MoviesSort,
		activeFilter: MoviesSort && MoviesSort.length && MoviesSort[0],
	},
	moviesCount: 10,
	moviesSort: {
		filters: MoviesSort,
		selectedFilter: MoviesSort && MoviesSort.length && MoviesSort[0],
	},
};

export default MovieSearch;