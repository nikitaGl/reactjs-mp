export const ReducersDefault = {
	movies: {
		movies: [],
		moviesAreLoading: false,
		moviesHasError: false,
		moviesSort: {
			filters: [
				{
					label: "release date",
					value: "release_date",
				},
				{
					label: "rating",
					value: "vote_average",
				},
			],
			selectedFilter: {
				label: "release date",
				value: "release_date",
			},
		},
		searchFilters: {
			activeFilter: {
				label: "title",
				value: "title",
			},
			filters: [
				{
					label: "title",
					value: "title",
				},
				{
					label: "genre",
					value: "genres",
				},
			],
		},
	},
};

export const SearchFilterReducerDefault = {
	activeFilter: {
		label: "title",
		value: "title",
	},
	filters: [
		{
			label: "title",
			value: "title",
		},
		{
			label: "genre",
			value: "genres",
		},
	],
};

export const MovieSortReducerDefault = {
	filters: [
		{
			label: "release date",
			value: "release_date",
		},
		{
			label: "rating",
			value: "vote_average",
		},
	],
	selectedFilter: {
		label: "release date",
		value: "release_date",
	},
};