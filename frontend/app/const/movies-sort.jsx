const MoviesSort = [
	{
		label: 'release date',
		value: 'release_date',
	},
	{
		label: 'rating',
		value: 'vote_average',
	}
];

export default MoviesSort;