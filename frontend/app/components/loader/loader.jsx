import React from 'react';

import './loader.scss';

class LoaderComponent extends React.Component {
	render() {
		return (
			<div className="loader">Loading...</div>
		);
	}
}

export default LoaderComponent;