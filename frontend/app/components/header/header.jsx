import React from 'react';

import './header.scss';

class HeaderComponent extends React.PureComponent {
	render() {
		return (
			<header className="header">
				<div className="header__title">netflixroulette</div>
			</header>
		);
	}
}

export default HeaderComponent;