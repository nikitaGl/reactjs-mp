// @flow

import React from 'react';
import { Link } from 'react-router-dom';

import './film-header.scss';
import FilmInfo from './components/film-info/film-info';

type IFilmHeader = {
	filmInfo: IMovie,
};

export class FilmHeader extends React.Component<IFilmHeader> {
	render() {
		const { filmInfo } = this.props;
		return (
			<div className="film-header">
				<div className="film-header__title-wrapper">
					<div className="film-header__title">netflixroulette</div>
					<Link to="/"><button className="button button--white film-header__search-button">Search</button></Link>
				</div>
				{ filmInfo ? <FilmInfo filmInfo={filmInfo} /> : false }
			</div>
		);
	}
}

export default FilmHeader;