// @flow

import React from 'react';

import './film-info.scss';

type IFilmInfo = {
	filmInfo: IMovie,
};

export class FilmInfo extends React.Component<IFilmInfo> {
	render() {
		const { poster_path, overview, release_date, title } = this.props.filmInfo;
		return (
			<div className="film-info">
				<img className="film-info__poster" src={poster_path} />
				<div className="film-info__description">
					<div className="film-info__description__title">{title}</div>
					<div className="film-info__description__details">{FilmInfo.getYearFromDate(release_date)}</div>
					<div className="film-info__description__overview">{overview}</div>
				</div>
			</div>
		);
	}

	static getYearFromDate(date: string): string {
		return date && date.split('-')[0];
	}
}

export default FilmInfo;