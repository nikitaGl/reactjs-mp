// @flow

import React from 'react';

import './search-result.scss';

type ISearchResult = {
	film: IMovie,
};

class SearchResult extends React.Component<ISearchResult> {
	render() {
		const { film } = this.props;
		return (
			<div className="search-result">
				Films by {film.genres && film.genres[0]}  genre
			</div>
		);
	}
}

export default SearchResult;