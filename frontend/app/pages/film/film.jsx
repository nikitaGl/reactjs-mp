// @flow

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import './film.scss';
import FilmHeader from './components/film-header/film-header';
import * as actions from '../../actions';
import MovieList from '../movie/components/movie-list/movie-list';
import SearchResult from './components/search-result/search-result';

type IFilmPage = {
	film: IFilm,
	movies: IMovieList,
	location: any,
	actions: any,
};

export class FilmPage extends React.Component<IFilmPage> {
	componentDidMount() {
		this.loadEverything();
	}

	componentDidUpdate(prevProps: IFilmPage) {
		if (prevProps.location.pathname !== this.props.location.pathname) {
			this.loadEverything();
		}
	}

	loadEverything() {
		const { filmFetchData } = this.props.actions;
		const movieId = this.props.location.pathname.split('/')[2];
		filmFetchData(movieId);
	}

	render() {
		const { film, loading } = this.props.film.filmData;
		const { movies } = this.props.movies.moviesData;

		return (
			<React.Fragment>
				{
					!loading ?
						<React.Fragment>
							<FilmHeader filmInfo={film}/>
							<SearchResult film={film} />
							<MovieList movies={movies} />
						</React.Fragment> : false
				}
			</React.Fragment>
		);
	}
}

const mapStateToProps = (state) => ({...state});

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(FilmPage);
