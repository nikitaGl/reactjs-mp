import React from 'react';

import './not-found-page.scss';

const NotFoundPage = () => (
	<div className="not-found-page">
		<div className="not-found-page__title">Page not found!</div>
	</div>
);

export default NotFoundPage;