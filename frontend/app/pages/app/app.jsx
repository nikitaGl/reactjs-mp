import React from 'react';
import { hot } from 'react-hot-loader';
import { Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import './app.scss';
import '../../utils/polyfills';

import FooterComponent from '../../components/footer/footer';
import NotFoundPage from '../not-found/not-found';
import MoviePage from '../movie/movie';
import FilmPage from '../film/film';

const Root = ({
  Router, location, context, store,
}) => (
	<Provider store={store}>
		<Router location={location} context={context}>
			<React.Fragment>
				<div className="page-container">
					<Switch>
						<Route exact path="/" component={MoviePage} />
						<Route exact path="/film/:id" component={FilmPage} />
						<Route path="*" component={NotFoundPage} />
					</Switch>
				</div>
				{ FooterComponent }
			</React.Fragment>
		</Router>
	</Provider>
);

export default hot(module)(Root);
