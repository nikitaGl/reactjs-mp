// @flow

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createSelector, createSelectorCreator, defaultMemoize } from 'reselect';
import { isEqual } from 'lodash';

import MovieList from './components/movie-list/movie-list';
import MovieHeader from './components/movie-header/movie-header';
import ErrorBoundary from '../../components/error-boundary/error-boundary';
import NoMovies from './components/no-movies/no-movies';
import LoaderComponent from '../../components/loader/loader';
import './movie.scss';
import * as actions from '../../actions';

type IMoviePage = {
	movies: IMovieList,
	location: any,
	actions: any,
};

export class MoviePage extends React.Component<IMoviePage> {
	componentDidMount() {
		const { moviesFetchData } = this.props.actions;
		const { search } = this.props.location;
		moviesFetchData(search);
	}

	render() {
		const { moviesData, searchFilters, moviesSort } = this.props.movies;
		const { movies, loading } = moviesData;
		const { actions } = this.props;
		return (
			<ErrorBoundary>
				<MovieHeader actions={actions}
										 searchFilters={searchFilters}
										 moviesCount={movies && movies.length || 0}
										 moviesSort={moviesSort} />
				<div className="movie-page">
					<div className="movie-page__list-wrap">
						{
							loading ? <LoaderComponent />
								: movies && movies.length ? <MovieList movies={movies} /> : <NoMovies />
						}
					</div>
				</div>
			</ErrorBoundary>
		);
	}
}

const mapDispatchToProps = (dispatch) => ({
	actions: bindActionCreators(actions, dispatch),
});

const createDeepEqualSelector = createSelectorCreator(
	defaultMemoize,
	isEqual,
);

const getState = (state) => state;

const mapStateToProps = createDeepEqualSelector(
	[getState],
	(state) => {
		return {...state}
	},
);

export default connect(mapStateToProps, mapDispatchToProps)(MoviePage);