// @flow

import React from 'react';
import './movie-header.scss';
import MovieSearch from '../movie-search/movie-search';

type IMovieHeader = {
	searchFilters: ISearchFilter,
	moviesSort: IMoviesSort,
	moviesCount: number,
	actions: any,
};

class MovieHeader extends React.PureComponent<IMovieHeader> {
	render() {
		const { actions, searchFilters, moviesCount, moviesSort } = this.props;
		return (
			<header className="movie-header">
				<div className="movie-header__title">netflixroulette</div>
				<div className="movie-header__search">
					<MovieSearch actions={actions}
											 searchFilters={searchFilters}
											 moviesCount={moviesCount}
											 moviesSort={moviesSort} />
				</div>
			</header>
		);
	}
}

export default MovieHeader;