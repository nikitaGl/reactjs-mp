// @flow

import React from 'react';
import './card-text-block.scss';

type IInfo = {
	title: string,
	date: string,
	genres: Array<string>,
}

type ICardTextBlock = {
	info: IInfo,
};

class CardTextBlock extends React.Component<ICardTextBlock> {
	getYearFromDate: (data: string) => string;

	constructor() {
		super();
		this.getYearFromDate = CardTextBlock.getYearFromDate.bind(this);
	}

	static getYearFromDate(date: string): string {
		return date.split('-')[0];
	}

	render() {
		const {info} = this.props;
		return (
			<div className="card-text-block">
				<div className="card-text-block__header">
					<div className="card-text-block__title">{info.title}</div>
					<div className="card-text-block__date">{this.getYearFromDate(info.date)}</div>
				</div>
				<div className="card-text-block__genres">
					{info.genres.map((genre) => <div className="card-text-block__genres__genre" key={genre}>{genre}</div>)}
				</div>
			</div>
		);
	}
}

export default CardTextBlock;