// @flow

import React from 'react';
import { Link } from 'react-router-dom';
import injectSheet from 'react-jss';

import CardTextBlock from './components/card-text-block/card-text-block';

type IMovieCard = {
	movie: IMovie,
	classes: any,
};

const styles = {
	movieCard: {
		maxWidth: '200px',
		margin: '10px',
		transitionDuration: '0.3s',
		textDecoration: 'none',
		color: '#000000',

		'&:hover': {
			transform: 'scale(1.1)',
			cursor: 'pointer',
		},
	},
	poster: {
		maxWidth: '100%',
		height: 'auto',
	},
};

class MovieCard extends React.Component<IMovieCard> {
	render() {
		const { movie, classes } = this.props;
		return (
			<Link to={`/film/${movie.id}`} className={classes.movieCard}>
				<img className={classes.poster} src={movie.poster_path} />
				<CardTextBlock info={{
					title: movie.title,
					date: movie.release_date,
					genres: movie.genres
				}}/>
			</Link>
		);
	}
}

export default injectSheet(styles)(MovieCard);