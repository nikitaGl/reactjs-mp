// @flow

import React from 'react';
import { withRouter } from 'react-router';

import './movie-search.scss';
import SearchFilters from './components/search-filters/search-filters';
import SearchResult from './components/search-result/search-result';
import { addQueryParams } from '../../../../utils/router-utils';

type IMovieSearchProps = {
	searchFilters: ISearchFilter,
	moviesSort: IMoviesSort,
	moviesCount: number,
	actions: any,
	location: any,
};

type IMovieSearchState = {
	searchField: string,
}

class MovieSearch extends React.Component<IMovieSearchProps, IMovieSearchState> {
	doSearch: () => void;
	updateSearchField: (event: any) => void;

	constructor(props: IMovieSearchProps) {
		super(props);
		const { search } = this.props.location;
		const searchParams = new URLSearchParams(search);
		const searchField = searchParams.get('search');
		this.state = {
			searchField: searchField || '',
		};
		this.updateSearchField = this.updateSearchField.bind(this);
		this.doSearch = this.doSearch.bind(this);
	}

	updateSearchField(event: any) {
		this.setState({
			searchField: event.target.value,
		});
	}

	doSearch() {
		const { moviesFetchData } = this.props.actions;
		const { searchFilters, moviesSort } = this.props;
		const { searchField } = this.state;
		addQueryParams({
			search: searchField,
		});
		moviesFetchData(`?search=${searchField}&searchBy=${searchFilters.activeFilter.value}&sortBy=${moviesSort.selectedFilter.value}&sortOrder=desc`);
	}

	render() {
		const { searchField } = this.state;
		const { actions, searchFilters, moviesCount, moviesSort } = this.props;
		return (
			<React.Fragment>
				<div className="movie-search">
					<input className="movie-search__search-field" type="text"
								 value={searchField} onChange={this.updateSearchField} />
					<div className="movie-search__sub-wrap">
						<SearchFilters actions={actions} searchFilters={searchFilters} />
						<button className="button movie-search__search-button" onClick={this.doSearch}>search</button>
					</div>
				</div>
				<SearchResult actions={actions} searchResult={{moviesCount}} moviesSort={moviesSort} />
			</React.Fragment>
		);
	}
}

export default withRouter(MovieSearch);