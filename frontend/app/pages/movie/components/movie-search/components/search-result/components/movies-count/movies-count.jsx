// @flow

import React from 'react';

import './movies-count.scss';

type IMoviesCountProps = {
	moviesCount: number,
};

class MoviesCount extends React.Component<IMoviesCountProps> {
	render() {
		const {moviesCount} = this.props;
		return (
			<div className="movies-count">{moviesCount && moviesCount + ' movies found'}</div>
		);
	}
}

export default MoviesCount;