// @flow

import React from 'react';
import classNames from 'classnames';
import { withRouter } from 'react-router';

import './movies-sort.scss';
import { addQueryParams } from '../../../../../../../../utils/router-utils';

type IMoviesSortProps = {
	moviesSort: IMoviesSort,
	actions: any,
	location: any,
};

class MoviesSort extends React.Component<IMoviesSortProps> {
	constructor(props: IMoviesSortProps) {
		super(props);
		const { search } = this.props.location;
		const searchParams = new URLSearchParams(search);
		const sortByField = searchParams.get('sortBy');
		if (sortByField) {
			const { filters, selectedFilter } = this.props.moviesSort;
			const queryFilter = filters.find((filter) => filter.value === sortByField);
			if (queryFilter && selectedFilter.value !== queryFilter.value) {
				this.selectSorting(queryFilter);
			}
		}
	}

	selectSorting(selectedFilter) {
		addQueryParams({
			sortBy: selectedFilter.value,
			sortOrder: 'desc',
		});
		const { selectSorting } = this.props.actions;
		selectSorting(selectedFilter);
	}

	render() {
		const {filters, selectedFilter} = this.props.moviesSort;
		return (
			<div className="movies-sort">
				<div className="movies-sort__title">Sort by</div>
				{
					filters
						.map((filter) => {
							const sortFilterClasses = classNames({
								'movies-sort__filter': true,
								'movies-sort__filter--active': selectedFilter.value === filter.value,
							});
							return <div className={sortFilterClasses} key={filter.value}
													onClick={() => this.selectSorting(filter)}>{filter.label}</div>;
						})
				}
			</div>
		);
	}
}

export default withRouter(MoviesSort);