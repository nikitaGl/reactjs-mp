// @flow

import React from 'react';

import MoviesCount from './components/movies-count/movies-count';
import MoviesSort from './components/movies-sort/movies-sort';
import './search-result.scss';

type ISearchResult = {
	moviesCount: number,
}

type ISearchResultProps = {
	moviesSort: IMoviesSort,
	searchResult: ISearchResult,
	actions: any,
}

class SearchResult extends React.Component<ISearchResultProps> {
	render() {
		const { searchResult, moviesSort, actions } = this.props;
		return (
			<div className="search-result">
				<MoviesCount moviesCount={searchResult && searchResult.moviesCount}/>
				<MoviesSort actions={actions} moviesSort={moviesSort} />
			</div>
		);
	}
}

export default SearchResult;