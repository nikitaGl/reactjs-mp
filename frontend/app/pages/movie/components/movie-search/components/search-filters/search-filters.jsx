// @flow

import React from 'react';
import classNames from 'classnames';
import { withRouter } from 'react-router';

import './search-filters.scss';
import { addQueryParams } from '../../../../../../utils/router-utils';

type ISearchFiltersProps = {
	searchFilters: ISearchFilter,
	location: any,
	actions: any,
}

class SearchFilters extends React.Component<ISearchFiltersProps> {
	constructor(props: ISearchFiltersProps) {
		super(props);
		const { search } = this.props.location;
		const searchParams = new URLSearchParams(search);
		const searchByField = searchParams.get('searchBy');
		if (searchByField) {
			const { filters, activeFilter } = this.props.searchFilters;
			const queryFilter = filters.find((filter) => filter.value === searchByField);
			if (queryFilter && activeFilter.value !== queryFilter.value) {
				this.selectFilter(queryFilter);
			}
		}
	}

	selectFilter(selectedFilter: ILabelValue): void {
		addQueryParams({
			searchBy: selectedFilter.value,
		});
		const { selectFilter } = this.props.actions;
		selectFilter(selectedFilter);
	}

	render() {
		const { filters, activeFilter } = this.props.searchFilters;
		return (
			<div className="search-filters">
				<div className="search-filters__title">search by</div>
				{
					filters
						.map((filter) => {
							const buttonClasses = classNames({
								'button': true,
								'button--inactive': activeFilter.value !== filter.value,
							});
							return <button
								className={buttonClasses}
								key={filter.value}
								onClick={() => this.selectFilter(filter)}>{filter.label}</button>;
						})
				}
			</div>
		);
	}
}

export default withRouter(SearchFilters);