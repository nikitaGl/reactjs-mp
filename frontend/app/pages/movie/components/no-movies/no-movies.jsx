import React from 'react';
import injectSheet from 'react-jss';

const styles = {
	noMovies: {
		display: 'flex',
		justifyContent: 'center',
	},
};

class NoMovies extends React.Component {
	render() {
		const { classes } = this.props;
		return (
			<div className={classes.noMovies}>No movies found!</div>
		);
	}
}

export default injectSheet(styles)(NoMovies);