// @flow

import React from 'react';
import injectSheet from 'react-jss';

import MovieCard from '../movie-card/movie-card';

const styles = {
	movieList: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'center',
	},
};

type IMovieList = {
	movies: Array<IMovie>,
	classes: any,
};

class MovieList extends React.Component<IMovieList> {
	render() {
		const { movies, classes } = this.props;
		return (
			<div className={classes.movieList}>
				{movies.map((movie) => <MovieCard key={movie.id} movie={movie} />)}
			</div>
		);
	}
}

export default injectSheet(styles)(MovieList);