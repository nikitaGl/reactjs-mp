import { FILM_HAS_ERROR, FILM_FETCH_DATA_SUCCESS, FILM_FETCH_DATA } from '../../const/action-types';

const INITIAL_STATE = {
	film: {},
	loading: false,
	hasError: false,
};

export default function filmReducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case FILM_HAS_ERROR:
			return {
				...state,
				hasError: action.hasError,
			};
		case FILM_FETCH_DATA:
			return {
				...state,
				loading: true,
			};
		case FILM_FETCH_DATA_SUCCESS:
			return {
				...state,
				film: action.film,
				loading: false,
			};
		default:
			return state;
	}
};
