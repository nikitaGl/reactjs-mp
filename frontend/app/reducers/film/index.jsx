import { combineReducers } from 'redux';

import filmReducer from './film';

export default combineReducers({
	filmData: filmReducer,
});