import { combineReducers } from 'redux';
import movies from './movie';
import film from './film';

export default combineReducers({
	movies,
	film,
});