import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';

import rootReducer from './index';
import rootSaga from '../sagas';

const sagaMiddleware = createSagaMiddleware();

export default (initialState) => {
	const store = createStore(rootReducer, initialState, applyMiddleware(sagaMiddleware));

	sagaMiddleware.run(rootSaga);
	store.runSaga = () => sagaMiddleware.run(rootSaga);
	store.close = () => store.dispatch(END);

	return store;
};
