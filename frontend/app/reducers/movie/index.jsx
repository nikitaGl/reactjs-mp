import { combineReducers } from 'redux';

import movieReducer from './movie';
import searchFilters from './search-filters';
import moviesSort from './movies-sort';

export default combineReducers({
	moviesData: movieReducer,
	searchFilters,
	moviesSort,
});