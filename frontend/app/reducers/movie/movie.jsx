import { MOVIES_HAS_ERROR, MOVIES_FETCH_DATA_SUCCESS, MOVIES_FETCH_DATA } from '../../const/action-types';

const INITIAL_STATE = {
	movies: [],
	loading: false,
	hasError: false,
};

export default function movieReducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case MOVIES_HAS_ERROR:
			return {
				...state,
				hasError: action.hasError,
			};
		case MOVIES_FETCH_DATA:
			return {
				...state,
				loading: true,
			};
		case MOVIES_FETCH_DATA_SUCCESS:
			return {
				...state,
				movies: action.movies,
				loading: false,
			};
		default:
			return state;
	}
};
