import { SELECT_MOVIE_SORT } from '../../const/action-types';
import MoviesSort from '../../const/movies-sort';

const initState = {
	filters: MoviesSort,
	selectedFilter: MoviesSort && MoviesSort.length && MoviesSort[0],
};

export default (state = initState, action) => {
	switch (action.type) {
		case SELECT_MOVIE_SORT:
			return {
				...state,
				selectedFilter: action.selectedFilter,
			};
		default:
			return state;
	}
};