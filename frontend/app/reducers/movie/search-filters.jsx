import { SELECT_SEARCH_FILTER } from '../../const/action-types';
import SearchFilters from '../../const/search-filters';

const initState = {
	filters: SearchFilters,
	activeFilter: SearchFilters && SearchFilters.length && SearchFilters[0],
};

export default (state = initState, action) => {
	switch (action.type) {
		case SELECT_SEARCH_FILTER:
			return {
				...state,
				activeFilter: action.activeFilter,
			};
		default:
			return state;
	}
};