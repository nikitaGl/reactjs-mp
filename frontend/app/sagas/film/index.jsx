import { call, put, all, takeLatest } from 'redux-saga/effects';

import { FILM_FETCH_DATA } from '../../const/action-types';
import { filmFetchDataSuccess, moviesFetchData } from '../../actions';
import Config from '../../config/endpoints';

function* filmFetchDataAsync(action) {
	const { id } = action;
	const response = yield call(fetch, `${Config.baseUrl}${Config.movies}/${id}`);
	const film = yield response.json();
	yield put(filmFetchDataSuccess(film));

	const [genre] = film.genres;
	const moviesQuery = new URLSearchParams();
	moviesQuery.set('searchBy', 'genres');
	moviesQuery.set('search', genre);
	yield put(moviesFetchData(`?${moviesQuery.toString()}`));
}

function* watchFilmFetchData() {
	yield takeLatest(FILM_FETCH_DATA, filmFetchDataAsync);
}

export default function* filmSaga() {
	yield all([
		watchFilmFetchData(),
	]);
}