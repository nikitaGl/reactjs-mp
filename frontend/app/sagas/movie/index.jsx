import { call, put, all, takeLatest } from 'redux-saga/effects';

import { MOVIES_FETCH_DATA } from '../../const/action-types';
import { moviesFetchDataSuccess } from '../../actions';
import Config from '../../config/endpoints';

function* moviesFetchDataAsync(action) {
	const { query } = action;
	const response = yield call(fetch, `${Config.baseUrl}${Config.movies}${query || ''}`);
	const movies = yield response.json();
	yield put(moviesFetchDataSuccess(movies.data));
}

function* watchMoviesFetchData() {
	yield takeLatest(MOVIES_FETCH_DATA, moviesFetchDataAsync);
}

export default function* movieSaga() {
	yield all([
		watchMoviesFetchData(),
	]);
}