import { all } from 'redux-saga/effects';

import movieSaga from './movie';
import filmSaga from './film';

export default function* rootSaga() {
	yield all([
		filmSaga(),
		movieSaga(),
	]);
};
