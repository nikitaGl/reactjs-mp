import React from 'react';
import { hydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import Root from './pages/app/app';
import configureStore from './reducers/configureStore';
import './shared/styles.scss'

const store = configureStore(window.PRELOADED_STATE);

const root = (
	<Root
		Router={BrowserRouter}
		store={store}
	/>
);

hydrate(root, document.getElementById('app'));
